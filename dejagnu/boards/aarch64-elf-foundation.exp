# Load the generic configuration for this board. This will define a basic
# set of routines used to communicate with the board.
load_generic_config "qemu"

# No multilib flags needed by default.
process_multilib_options ""

if { [board_info $board obj_format] == "pe" } {
    set additional_options "-Wl,-oformat,pe-arm-little,--image-base,0"
    #    set_board_info uses_underscores 1
} else {

    #    if [istarget "*-*-coff"] {
    #        set_board_info uses_underscores 1
    #    }

    set additional_options ""
}

# We only support newlib on this target. We assume that all multilib
# options have been specified before we get here.
set_board_info compiler "[find_gcc]"
set_board_info cflags	"-specs=aem-ve.specs [libgloss_include_flags] [newlib_include_flags]"
set_board_info ldflags	"[libgloss_link_flags] [newlib_link_flags] $additional_options"
set_board_info sim      "models-run"

case "$target_triplet" in {
    { "aarch64_be-*-elf" } {
	set_board_info sim,options "--model=foundation --arch=armv8.6-a --bigendian"
    }
    default {
	set_board_info sim,options "--model=foundation --arch=armv8.6-a"
    }
}

set_board_info gdb,skip_huge_test 1
set_board_info is_simulator 1

# Used by a few gcc.c-torture testcases to delimit how large the stack can
# be.
#set_board_info gcc,stack_size  16384

# More time is needed to compile PlumHall tests
#set_board_info gcc,timeout 800
# newlib strcmp-1.c test on AEM requires > 240s
set_board_info qemu_time_limit 240

# Runtime doesn't pass signals
set_board_info gdb,nosignals 1

# QEMU doesn't allow hardware watchpoints.
set_board_info gdb,no_hardware_watchpoints 1

# Don't expect interrupts to work in gdb testsuite
proc set_host_info { entry value } {
    global target_info board_info
    verbose "set_host_info $entry $value" 3

    set machine host
    if {[info exists target_info($machine,name)]} {
        set machine $target_info($machine,name)
    }
    set board_info($machine,$entry) $value
}

# Cannot pass command line arguments
set_board_info noargs 1
